terraform {
  required_providers {
    restapi = {
      source  = "Mastercard/restapi"
      version = "1.19.1"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "4.26.0"
    }
  }
}

provider "restapi" {
  alias                = "cloudflare"
  uri                  = "https://api.cloudflare.com"
  write_returns_object = true
  debug                = true

  headers = {
    "Authorization" = "Bearer ${var.cloudflare_api_token}",
    "Content-Type"  = "application/json"
  }

  create_method  = "PUT"
  update_method  = "PUT"
  destroy_method = "PUT"
}


data "cloudflare_zone" "zone" {
  zone_id = var.zone_id
}

locals {
  domain     = "${var.subdomain}${var.subdomain == "" ? "" : "."}${data.cloudflare_zone.zone.name}"
  account_id = data.cloudflare_zone.zone.account_id
}

resource "restapi_object" "static-domain-mapping" {
  provider = restapi.cloudflare
  path     = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/custom_domains"
  data = jsonencode({
    domain   = local.domain
    zoneId   = data.cloudflare_zone.zone.zone_id
    zoneName = data.cloudflare_zone.zone.name
  })

  id_attribute = "domain"

  read_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/custom_domains"
  read_method = "GET"
  read_data   = ""
  read_search = {
    search_key   = "domain"
    search_value = local.domain
    results_key  = "result/domains"
  }

  create_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/custom_domains"
  create_method = "POST"

  update_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/custom_domains/${local.domain}"
  update_method = "PUT"

  destroy_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/custom_domains/${local.domain}"
  destroy_method = "DELETE"

  force_new = [local.domain]
}

resource "restapi_object" "static-domain-policy" {
  provider = restapi.cloudflare
  path     = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/policy?cname=${local.domain}&access=CnamesOnly"
  data     = ""

  object_id = local.domain

  read_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/policy"
  read_method = "GET"
  read_data   = ""

  create_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/policy?cname=${local.domain}&access=CnamesOnly"
  create_method = "PUT"

  destroy_path   = "/client/v4/accounts/${local.account_id}/r2/buckets/${var.bucket}/policy?cname=&access=CnamesOnly"
  destroy_method = "PUT"
  destroy_data   = ""

  force_new = [local.domain]

  depends_on = [restapi_object.static-domain-mapping]
}
