variable "zone_id" {
  type        = string
  description = "CloudFlare Zone ID"
}

variable "cloudflare_api_token" {
  type        = string
  description = "CloudFlare API Token"
}

variable "bucket" {
  type        = string
  description = "R2 Bucket Name"
}

variable "subdomain" {
  type        = string
  description = "Subdomain within zone"
}
